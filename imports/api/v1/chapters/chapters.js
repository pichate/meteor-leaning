import {
    Meteor
} from 'meteor/meteor';
import {
    Mongo
} from 'meteor/mongo';
import {
    check
} from 'meteor/check';

export const Chapters = new Mongo.Collection('chapters');

if (Meteor.isServer) {

    // Global API configuration
    var Api = new Restivus({
        apiPath: 'api/',
        version: 'v1',
        useDefaultAuth: false,
        prettyJson: true,
    });

    // Generates: GET, POST on /api/v1/chapters and GET, PUT, DELETE on
    // /api/v1/chapters/:id for the Chapters collection
    // Api.addCollection(Chapters);

    Api.addRoute('chapters', {
        authRequired: true
    }, {
        get: {
            authRequired: false,
            action: function () {
                const chapters = JSON.parse(Assets.getText("chapters.json"));
                // GET api/v1/chapters
                return {
                    statusCode: 200,
                    headers: {
                        // 'Content-Type': 'text/plain',
                        'X-Custom-Header': 'custom value'
                    },
                    // Add 'body' if you want to return raw html
                    // body: '<h1>Hello World</h1>',

                    // Remove body and content-type above if you want to return as JSON
                    data: chapters.data
                };
            }
        },
        post: function () {
            // POST api/v1/chapters
        },
        put: function () {
            // PUT api/v1/chapters
        },
        patch: function () {
            // PATCH api/v1/chapters
        },
        delete: function () {
            // DELETE api/v1/chapters
        },
        options: function () {
            // OPTIONS api/v1/chapters
        }
    });
}
